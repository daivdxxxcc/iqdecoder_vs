﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IQTracePlot
{
    public class IQDecoder
    {
        private byte[] base64Decoded_byte;
        private short[,] IQData;
        private double[] ampData, timeTrace;
        private int arrLength;

        //Constructor
        public IQDecoder()
        {

        }

        //Decode the encoded qi data and time trace
        public void decodeIQ(string encodedQIData, double freq)
        {
            //Decode the qi_data with base64 decoder
            base64Decoded_byte = Convert.FromBase64String(encodedQIData);
            //every set of I and Q data is short int with 2 bytes, so 2 (I)+ 2(Q) = 4 bytes
            // ArrLength tell us how many set of IQ data point we have from the decoded byte array
            arrLength = base64Decoded_byte.Length / 4;
            calIQData(base64Decoded_byte);
            genTimeTrace(arrLength, freq);

        }

        //Combine every 2 byte as short integer for decode iq trace and update IQData, ampData
        private void calIQData(byte[] byteArr)
        {
            double I, Q = 0;
            IQData = new short[arrLength, 2];
            ampData = new double[arrLength];
            for (int i = 0; i < arrLength; i++)
            {
                IQData[i, 0] = BitConverter.ToInt16(byteArr, i * 4);
                IQData[i, 1] = BitConverter.ToInt16(byteArr, i * 4 + 2);
                I = IQData[i, 0];
                Q = IQData[i, 1];
                ampData[i] = Math.Sqrt(Math.Pow(I, 2) + Math.Pow(Q, 2));
            }
        }

        //Generate time trace based on the input of byteArray and frequency
        private void genTimeTrace(int arrLen, double freq)
        {
            //Sample size is freq/8
            double timeStep = 8.0 / freq;
            timeTrace = new double[arrLen];
            for (int i = 0; i < arrLength; i++)
            {
                timeTrace[i] = (double)(i * timeStep);
            }
        }

        //Return set of IQ data as 2D array
        public short[,] getIQData
        {
            get
            {
                return this.IQData;
            }
        }

        //Return amplitude data as 1D array
        public double[] getAmpData
        {
            get
            {
                return this.ampData;
            }
        }

        //Return timestamp data as 1D array 
        public double[] getTimeTrace
        {
            get
            {
                return this.timeTrace;
            }
        }

        public int getArrLength
        {
            get
            {
                return this.arrLength;
            }
        }

    }

    public class IQDataHandler
    {

        public IQDataHandler()
        {

        }


        public void plotIQTrace()
        {

        }
    }

    public class DUTData
    {

        private List<string> _DUT;
        private List<string> _die_id;
        private List<string> _encoded_qi_data;
        private List<double> _freq;

        public DUTData(List<string> dut, List<string> die_id, List<string> encoded_qi_data, List<double> freq)
        {
            this._DUT = dut;
            this._die_id = die_id;
            this._encoded_qi_data = encoded_qi_data;
            this._freq = freq;
        }

        public List<string> DUT
        {
            get { return _DUT; }
            set { _DUT = value; }
        }
        public List<string> die_id
        {
            get { return _die_id; }
            set { _die_id = value; }
        }
        public List<string> encoded_qi_data
        {
            get { return _encoded_qi_data; }
            set { _encoded_qi_data = value; }
        }
        public List<double> freq
        {
            get { return _freq; }
            set { _freq = value; }
        }

    }

   


   // public class Program : Form1
    public class Program     
    {
        //Form1 PassForm;
        public static Program _dataHandler = null;

        public static Program dataHandler
        {
            get 
            {
                if (_dataHandler == null)
                {
                    _dataHandler = new Program();
                }
                return _dataHandler;
            }

        }



        public List<DUTData> testValiation()
        {
            //This is 201 IQ trace w/ 90 data set
            string encoded_qiDataString = "AAAAAHf+ngAd9d8E8eUXDWzjFhG88FQMKPxbBYT/jAH0/1EA/P8OAPz/BQAEAAkAFQAaABsAKwALACIA/f8OAP//+f87AFD/bQE0/QIEWfqoBrf4awcO+cUF1frvAhn9ugDc/uj/0v/q/zwACgBrAKv/UwGM/TsE+/hXCP/zBAu68QELX/PTCIX3sAVf++MCaP0AAS/+BACQ/r//6/7s/1T/LwDE/zwAOQAPAKcA0v/hAKn/xgCg/2UAtv/v//v/iP9ZAFb/jgBv/4YAt/9kAAAATwAqAEkAKAA6ABkAIwAYACIAEgBZAPz/xQDf/yABx/8vAb7/AAG9/6sAsP9MAJj/AgCW/+D/t//l/+T/8/8DAPL/CwDv//7/7v/s/97/7P/D//j/uv8HAMP/HwDV/zYA+/8zACQAGwAmAAoACgAMAO//HQDe/zkA4f9JAPr/RQAYAEMAMABNADkAXwArAG8AEABvAPn/";
            //This is 101 IQ trace w/ 60 data set
            //string base64Encoded = "AAAAAET/lAAq+ekECewVDQvkDBDF6TMKOfScA8j61QDB/RkAEP/v/6T/8f/g//v/8//7////+P8DAPr/+//6//j/9//6//r//v/9/wkA/v8PAAMABQAHAP7/AwAKAAUAEgAQAAwAEgAyAAIAjwB3AKkA0wH+/18Ca/7nAOP8Rv+p/Pb+J/1U/2r9d/8m/gD/jP9V/kcAHv6T/5X+pf0bAKP6NwJw96UCcfaHAOr4//1q/Lr81f3D/Kr9DP4B/rn/Ov94AMIAtwAGAhoCQAJ6BBYBWwWo/5kDg//7AC8Agf8SAEIAzP4hA7f8uQVu+hAG";
            //IQDecoder obj = new IQDecoder();
            //obj.decodeIQ(base64Encoded, 176000);
            List<string> DUTData = new List<string>();
            List<string> dieData = new List<string>();
            List<string> encoded_qiData = new List<string>();
            List<double> freqData = new List<double>();
            List<DUTData> DUTList = new List<DUTData>(4);
            //string encoded_qiDataString = "AAAAAET/lAAq+ekECewVDQvkDBDF6TMKOfScA8j61QDB/RkAEP/v/6T/8f/g//v/8//7////+P8DAPr/+//6//j/9//6//r//v/9/wkA/v8PAAMABQAHAP7/AwAKAAUAEgAQAAwAEgAyAAIAjwB3AKkA0wH+/18Ca/7nAOP8Rv+p/Pb+J/1U/2r9d/8m/gD/jP9V/kcAHv6T/5X+pf0bAKP6NwJw96UCcfaHAOr4//1q/Lr81f3D/Kr9DP4B/rn/Ov94AMIAtwAGAhoCQAJ6BBYBWwWo/5kDg//7AC8Agf8SAEIAzP4hA7f8uQVu+hAG";
            DUTData.Add("DUT1");
            dieData.Add("aaa");
            encoded_qiData.Add(encoded_qiDataString);
            freqData.Add(176123);

            DUTData site1 = new DUTData(DUTData, dieData, encoded_qiData, freqData);
            DUTData site2 = new DUTData(DUTData, dieData, encoded_qiData, freqData);
            DUTData site3 = new DUTData(DUTData, dieData, encoded_qiData, freqData);
            DUTData site4 = new DUTData(DUTData, dieData, encoded_qiData, freqData);

            DUTList.Add(site1);
            DUTList.Add(site2);
            DUTList.Add(site3);
            DUTList.Add(site4);

            return DUTList;


            //IQDecoder IQDecoderObj = new IQDecoder();
            //IQDecoderObj.decodeIQ(DUTList[0].encoded_qi_data[0], DUTList[0].freq[0]);


        }


        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            List<DUTData> dut = Program.dataHandler.testValiation();
            Form1 obj = new IQTracePlot.Form1(dut);
           // Form1 obj = new Form1(dut);
            Application.Run(obj);

            //obj.Form1_Load(testValiation()); 
        }




        ///// <summary>
        ///// The main entry point for the application.
        ///// </summary>
        //[STAThread]
        //static void Main()
        //{

        //    //This is 201 IQ trace w/ 90 data set
        //    //string base64Encoded = "AAAAAHf+ngAd9d8E8eUXDWzjFhG88FQMKPxbBYT/jAH0/1EA/P8OAPz/BQAEAAkAFQAaABsAKwALACIA/f8OAP//+f87AFD/bQE0/QIEWfqoBrf4awcO+cUF1frvAhn9ugDc/uj/0v/q/zwACgBrAKv/UwGM/TsE+/hXCP/zBAu68QELX/PTCIX3sAVf++MCaP0AAS/+BACQ/r//6/7s/1T/LwDE/zwAOQAPAKcA0v/hAKn/xgCg/2UAtv/v//v/iP9ZAFb/jgBv/4YAt/9kAAAATwAqAEkAKAA6ABkAIwAYACIAEgBZAPz/xQDf/yABx/8vAb7/AAG9/6sAsP9MAJj/AgCW/+D/t//l/+T/8/8DAPL/CwDv//7/7v/s/97/7P/D//j/uv8HAMP/HwDV/zYA+/8zACQAGwAmAAoACgAMAO//HQDe/zkA4f9JAPr/RQAYAEMAMABNADkAXwArAG8AEABvAPn/";
        //    //This is 101 IQ trace w/ 60 data set
        //    //string base64Encoded = "AAAAAET/lAAq+ekECewVDQvkDBDF6TMKOfScA8j61QDB/RkAEP/v/6T/8f/g//v/8//7////+P8DAPr/+//6//j/9//6//r//v/9/wkA/v8PAAMABQAHAP7/AwAKAAUAEgAQAAwAEgAyAAIAjwB3AKkA0wH+/18Ca/7nAOP8Rv+p/Pb+J/1U/2r9d/8m/gD/jP9V/kcAHv6T/5X+pf0bAKP6NwJw96UCcfaHAOr4//1q/Lr81f3D/Kr9DP4B/rn/Ov94AMIAtwAGAhoCQAJ6BBYBWwWo/5kDg//7AC8Agf8SAEIAzP4hA7f8uQVu+hAG";
        //    //IQDecoder obj = new IQDecoder();
        //    //obj.decodeIQ(base64Encoded, 176000);
        //    List<string> DUTData = new List<string>();
        //    List<string> dieData = new List<string>();
        //    List<string> encoded_qiData = new List<string>();
        //    List<double> freqData = new List<double>();
        //    List<DUTData> DUTList = new List<DUTData>(4);
        //    string encoded_qiDataString = "AAAAAET/lAAq+ekECewVDQvkDBDF6TMKOfScA8j61QDB/RkAEP/v/6T/8f/g//v/8//7////+P8DAPr/+//6//j/9//6//r//v/9/wkA/v8PAAMABQAHAP7/AwAKAAUAEgAQAAwAEgAyAAIAjwB3AKkA0wH+/18Ca/7nAOP8Rv+p/Pb+J/1U/2r9d/8m/gD/jP9V/kcAHv6T/5X+pf0bAKP6NwJw96UCcfaHAOr4//1q/Lr81f3D/Kr9DP4B/rn/Ov94AMIAtwAGAhoCQAJ6BBYBWwWo/5kDg//7AC8Agf8SAEIAzP4hA7f8uQVu+hAG";
        //    DUTData.Add("DUT1");
        //    dieData.Add("aaa");
        //    encoded_qiData.Add(encoded_qiDataString);
        //    freqData.Add(176123);

        //    DUTData site1 = new DUTData(DUTData, dieData, encoded_qiData, freqData);
        //    DUTData site2 = new DUTData(DUTData, dieData, encoded_qiData, freqData);
        //    DUTData site3 = new DUTData(DUTData, dieData, encoded_qiData, freqData);
        //    DUTData site4 = new DUTData(DUTData, dieData, encoded_qiData, freqData);

        //    DUTList.Add(site1);
        //    DUTList.Add(site2);
        //    DUTList.Add(site3);
        //    DUTList.Add(site4);


        //    IQDecoder IQDecoderObj = new IQDecoder();
        //    IQDecoderObj.decodeIQ(DUTList[0].encoded_qi_data[0], DUTList[0].freq[0]);

        //    //Application.EnableVisualStyles();
        //    //Application.SetCompatibleTextRenderingDefault(false);
        //    //Application.Run(new Form1(DUTList));

        //    Console.WriteLine(IQDecoderObj.getAmpData);
        //    Console.WriteLine(IQDecoderObj.getTimeTrace);

        //}





    }
}
