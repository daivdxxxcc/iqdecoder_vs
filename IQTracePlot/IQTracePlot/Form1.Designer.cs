﻿namespace IQTracePlot
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.DUT02 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.DUT03 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.DUT02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DUT03)).BeginInit();
            this.SuspendLayout();
            // 
            // DUT02
            // 
            chartArea1.Name = "ChartArea1";
            this.DUT02.ChartAreas.Add(chartArea1);
            this.DUT02.ChartAreas[0].AxisY.IsLogarithmic = true;
            legend1.Name = "Legend1";
            this.DUT02.Legends.Add(legend1);
            this.DUT02.Location = new System.Drawing.Point(48, 42);
            this.DUT02.Name = "DUT02";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Legend = "Legend1";
            series1.Name = "M_01";
            this.DUT02.Series.Add(series1);
            this.DUT02.Size = new System.Drawing.Size(700, 430);
            this.DUT02.TabIndex = 0;
            this.DUT02.Text = "chart2";
            title1.Name = "Title1";
            title1.Text = "DUT02";
            this.DUT02.Titles.Add(title1);
            this.DUT02.Click += new System.EventHandler(this.chart2_Click);
           
            // 
            // DUT03
            // 
            chartArea2.Name = "ChartArea1";
            this.DUT03.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.DUT03.Legends.Add(legend2);
            this.DUT03.Location = new System.Drawing.Point(829, 42);
            this.DUT03.Name = "DUT03";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "M_01";
            this.DUT03.Series.Add(series2);
            this.DUT03.Size = new System.Drawing.Size(700, 430);
            this.DUT03.TabIndex = 1;
            this.DUT03.Text = "chart3";
            title2.Name = "Title1";
            title2.Text = "DUT03";
            this.DUT03.Titles.Add(title2);
            this.DUT03.Click += new System.EventHandler(this.chart3_Click);
            // 
            // ICTraceChart
            // 
            this.ClientSize = new System.Drawing.Size(1578, 944);
            this.Controls.Add(this.DUT03);
            this.Controls.Add(this.DUT02);
            this.Name = "ICTraceChart";
            ((System.ComponentModel.ISupportInitialize)(this.DUT02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DUT03)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart DUT02;
        private System.Windows.Forms.DataVisualization.Charting.Chart DUT03;
    }
}

